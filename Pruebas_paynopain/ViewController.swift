//
//  ViewController.swift
//  Pruebas_paynopain
//
//  Created by JessePinkman on 28/09/15.
//  Copyright (c) 2015 JessePinkman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func FizzBuzz(sender: AnyObject) {
        println("\nTEST Fiz Buzz 1")
        for i in 1...100 {
            var output = String(i)
            if i % 3 == 0 {
                if i % 5 == 0 {
                    output = "FizzBuzz"
                }
                else {
                    output = "Fizz"
                }
            }
            else if i % 5 == 0 {
                output = "Buzz"
            }
            println(output)
        }
    }
   
    @IBAction func FizzBuzz2(sender: AnyObject) {
        println("\nTEST Fizz Buzz 2")
        var char3 : Character = "3"
        var char5 : Character = "5"
        for i in 1...100 {
            var output = String(i)
            var index = find(output, char3)
            if i % 3 == 0 || find(output, char3) != nil {
                if i % 5 == 0 || find(output, char5) != nil {
                    output = "FizzBuzz"
                }
                else {
                    output = "Fizz"
                }
            }
            else if i % 5 == 0 || find(output, char5) != nil {
                output = "Buzz"
            }
            println(output)
        }
    }
    
    @IBAction func Bowling(sender: AnyObject) {
        //We supose that the case entered is in the correct format.
        //var caseTxt = "XXXXXXXXXXXX"
        //var caseTxt = "9-9-9-9-9-9-9-9-9-9-"
        //var caseTxt = "5/5/5/5/5/5/5/5/5/5/5"
        var caseTxt = "5/X9-X3/4-X6/3-4/6"
        println("\nTEST Bowling with '" + caseTxt + "'")
        if caseTxt != "" {
            var n = count(caseTxt)
            var array = Array(caseTxt)
            var i = 0
            var score = 0
            var sum = 0
            var nRolls = 0
            while nRolls < 10 {
                if array[i] == "X" { //STRIKE
                    sum += 10
                    var j = i + 1
                    if array[j] == "X" {
                        sum += 10
                    }
                    else {
                        sum += String(array[j]).toInt()!
                        j += 1
                    }
                    j += 1
                    if array[j] == "X" {
                        sum += 10
                    }
                    else {
                        sum += String(array[j]).toInt()!
                    }
                }
                else {
                    var j = i + 1
                    if array[j] == "/" { //SPARE
                        sum += 10
                        j += 1
                        if array[j] == "X" {
                            sum += 10
                        }
                        else {
                            sum += String(array[j]).toInt()!
                        }
                    }
                    else { //MISS
                        sum += String(array[i]).toInt()!
                    }
                    i++
                }
                nRolls++
                i++
            }
            println(sum)
            
        }
    }
    
    
    @IBAction func GameOfLive(sender: AnyObject) {
        var caseMatrix = [[".", ".", ".", ".", ".", ".", ".", "."],
                            [".", ".", ".", ".", "*", ".", ".", "."],
                            [".", ".", ".", "*", "*", ".", ".", "."],
                            [".", ".", ".", ".", ".", ".", ".", "."]]
        var r = caseMatrix.count
        var c = caseMatrix[0].count
        
        println("Actual Generation")
        for i in 0...r-1 {
            var line = ""
            for j in 0...c-1 {
                line += caseMatrix[i][j] + " "
            }
            println(line)
        }
        
        var caseMatrixNextGeneration = Array<Array<String>>()
        for column in 0...r {
            caseMatrixNextGeneration.append(Array(count:c, repeatedValue:String()))
        }
        
        //i = 0, j = 0
        var nLives = 0
        if caseMatrix[0][1] == "*" {
            nLives++
        }
        if caseMatrix[1][1] == "*" {
            nLives++
        }
        if caseMatrix[1][0] == "*" {
            nLives++
        }
        if caseMatrix[0][0] == "*"{
            if nLives < 2 {
                caseMatrixNextGeneration[0][0] = "."
            }
            else {
                caseMatrixNextGeneration[0][0] = "*"
            }
        }
        else {
            if nLives == 3 {
                caseMatrixNextGeneration[0][0] = "*"
            }
            else {
                caseMatrixNextGeneration[0][0] = "."
            }
        }
        
        //i = 0, j = c - 1
        nLives = 0
        if caseMatrix[0][c - 2] == "*" {
            nLives++
        }
        if caseMatrix[1][c - 2] == "*" {
            nLives++
        }
        if caseMatrix[1][c - 1] == "*" {
            nLives++
        }
        if caseMatrix[0][c - 1] == "*" {
            if nLives < 2 {
                caseMatrixNextGeneration[0][c - 1] = "."
            }
            else {
                caseMatrixNextGeneration[0][c - 1] = "*"
            }
        }
        else {
            if nLives == 3 {
                caseMatrixNextGeneration[0][c - 1] = "*"
            }
            else {
                caseMatrixNextGeneration[0][c - 1] = "."
            }
        }
        
        //i = r - 1, j = 0
        nLives = 0
        if caseMatrix[r - 2][0] == "*" {
            nLives++
        }
        if caseMatrix[r - 2][1] == "*" {
            nLives++
        }
        if caseMatrix[r - 1][1] == "*" {
            nLives++
        }
        if caseMatrix[r - 1][0] == "*"{
            if nLives < 2 {
                caseMatrixNextGeneration[r - 1][0] = "."
            }
            else {
                caseMatrixNextGeneration[r - 1][0] = "*"
            }
        }
        else {
            if nLives == 3 {
                caseMatrixNextGeneration[r - 1][0] = "*"
            }
            else {
                caseMatrixNextGeneration[r - 1][0] = "."
            }
        }
        
        //i = r - 1, j = c - 1
        nLives = 0
        if caseMatrix[r - 2][c - 1] == "*" {
            nLives++
        }
        if caseMatrix[r - 2][c - 2] == "*" {
            nLives++
        }
        if caseMatrix[r - 1][c - 2] == "*" {
            nLives++
        }
        if caseMatrix[r - 1][c - 1] == "*"{
            if nLives < 2 {
                caseMatrixNextGeneration[r - 1][c - 1] = "."
            }
            else {
                caseMatrixNextGeneration[r - 1][c - 1] = "*"
            }
        }
        else {
            if nLives == 3 {
                caseMatrixNextGeneration[r - 1][c - 1] = "*"
            }
            else {
                caseMatrixNextGeneration[r - 1][c - 1] = "."
            }
        }
        
        //First row
        var i = 0
        for j in 1...c-2 {
            var nLives = 0
            var nDies = 0
            if caseMatrix[i][j - 1] == "*" {
                nLives++
            }
            if caseMatrix[i][j + 1] == "*" {
                nLives++
            }
            if caseMatrix[i + 1][j - 1] == "*" {
                nLives++
            }
            if caseMatrix[i + 1][j] == "*" {
                nLives++
            }
            if caseMatrix[i + 1][j + 1] == "*" {
                nLives++
            }
            if caseMatrix[i][j] == "*"{
                if nLives < 2 || nLives > 3 {
                    caseMatrixNextGeneration[i][j] = "."
                }
                else  {
                    caseMatrixNextGeneration[i][j] = "*"
                }
            }
            else {
                if nLives == 3 {
                    caseMatrixNextGeneration[i][j] = "*"
                }
                else {
                    caseMatrixNextGeneration[i][j] = "."
                }
            }
        }
        
        //Last row
        i = r - 1
        for j in 1...c-2 {
            var nLives = 0
            if caseMatrix[i - 1][j - 1] == "*" {
                nLives++
            }
            if caseMatrix[i - 1][j] == "*" {
                nLives++
            }
            if caseMatrix[i - 1][j + 1] == "*" {
                nLives++
            }
            if caseMatrix[i][j - 1] == "*" {
                nLives++
            }
            if caseMatrix[i][j + 1] == "*" {
                nLives++
            }
            if caseMatrix[i][j] == "*"{
                if nLives < 2 || nLives > 3 {
                    caseMatrixNextGeneration[i][j] = "."
                }
                else  {
                    caseMatrixNextGeneration[i][j] = "*"
                }
            }
            else {
                if nLives == 3 {
                    caseMatrixNextGeneration[i][j] = "*"
                }
                else {
                    caseMatrixNextGeneration[i][j] = "."
                }
            }
        }
        
        //First col
        var j = 0
        for i in 1...r-2 {
            var nLives = 0
            if caseMatrix[i - 1][j] == "*" {
                nLives++
            }
            if caseMatrix[i - 1][j + 1] == "*" {
                nLives++
            }
            if caseMatrix[i][j + 1] == "*" {
                nLives++
            }
            if caseMatrix[i + 1][j] == "*" {
                nLives++
            }
            if caseMatrix[i + 1][j + 1] == "*" {
                nLives++
            }
            if caseMatrix[i][j] == "*"{
                if nLives < 2 || nLives > 3 {
                    caseMatrixNextGeneration[i][j] = "."
                }
                else  {
                    caseMatrixNextGeneration[i][j] = "*"
                }
            }
            else {
                if nLives == 3 {
                    caseMatrixNextGeneration[i][j] = "*"
                }
                else {
                    caseMatrixNextGeneration[i][j] = "."
                }
            }
        }
        
        
        //Last col
        j = c - 1
        for i in 1...r-2 {
            var nLives = 0
            if caseMatrix[i - 1][j - 1] == "*" {
                nLives++
            }
            if caseMatrix[i - 1][j] == "*" {
                nLives++
            }
            if caseMatrix[i][j - 1] == "*" {
                nLives++
            }
            if caseMatrix[i + 1][j - 1] == "*" {
                nLives++
            }
            if caseMatrix[i + 1][j] == "*" {
                nLives++
            }
            if caseMatrix[i][j] == "*"{
                if nLives < 2 || nLives > 3 {
                    caseMatrixNextGeneration[i][j] = "."
                }
                else  {
                    caseMatrixNextGeneration[i][j] = "*"
                }
            }
            else {
                if nLives == 3 {
                    caseMatrixNextGeneration[i][j] = "*"
                }
                else {
                    caseMatrixNextGeneration[i][j] = "."
                }
            }
        }
        
        //Rest of the cells
        for i in 1...r-2 {
            for j in 1...c-2 {
                var nLives = 0
                if caseMatrix[i - 1][j - 1] == "*" {
                    nLives++
                }
                if caseMatrix[i - 1][j] == "*" {
                    nLives++
                }
                if caseMatrix[i - 1][j + 1] == "*" {
                    nLives++
                }
                if caseMatrix[i][j - 1] == "*" {
                    nLives++
                }
                if caseMatrix[i][j + 1] == "*" {
                    nLives++
                }
                if caseMatrix[i + 1][j - 1] == "*" {
                    nLives++
                }
                if caseMatrix[i + 1][j] == "*" {
                    nLives++
                }
                if caseMatrix[i + 1][j + 1] == "*" {
                    nLives++
                }
                if caseMatrix[i][j] == "*"{
                    if nLives < 2 || nLives > 3 {
                        caseMatrixNextGeneration[i][j] = "."
                    }
                    else  {
                        caseMatrixNextGeneration[i][j] = "*"
                    }
                }
                else {
                    if nLives == 3 {
                        caseMatrixNextGeneration[i][j] = "*"
                    }
                    else {
                        caseMatrixNextGeneration[i][j] = "."
                    }
                }
            }
        }
        
        println("\nNext Generation")
        for i in 0...r-1 {
            var line = ""
            for j in 0...c-1 {
                line += caseMatrixNextGeneration[i][j] + " "
            }
            println(line)
        }
    }
    
    
}

